package org.example.app.services;

import org.example.app.exceptions.BookShelfRegexException;

import java.util.List;

public interface ProjectRepository<T> {
    List<T> retreiveAll();

    void store(T book);

    boolean removeItemById(Integer bookIdToRemove);

    void removeByRegex(String queryRegex) throws BookShelfRegexException;
}
