package org.example.web.controllers;

import org.apache.log4j.Logger;
import org.example.app.exceptions.BookShelfFileUploadException;
import org.example.app.exceptions.BookShelfRegexException;
import org.example.app.services.BookService;
import org.example.web.dto.Book;
import org.example.web.dto.BookIdToRemove;
import org.example.web.dto.RegexToRemoveBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.*;

@Controller
@RequestMapping(value = "books")
public class BookShelfController {
    private Logger logger = Logger.getLogger(BookShelfController.class);
    private BookService bookService;

    @Autowired
    public BookShelfController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/shelf")
    public String books(Model model){
        logger.info("got book shelf");
        model.addAttribute("book", new Book());
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("regexToRemoveBook", new RegexToRemoveBook());
        model.addAttribute("bookList", bookService.getAllBooks());
        return "book_shelf";
    }
    @PostMapping("/save")
    public String saveBook(@Valid Book book, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            model.addAttribute("book", book);
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("regexToRemoveBook", new RegexToRemoveBook());
            model.addAttribute("bookList", bookService.getAllBooks());
            return "book_shelf";
        }else {
            bookService.saveBook(book);
            logger.info("current repository contents: " + bookService.getAllBooks().size());
            return "redirect:/books/shelf";
        }
    }

    @PostMapping("/remove")
    public String removeBook(@Valid BookIdToRemove bookIdToRemove, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            model.addAttribute("book", new Book());
            model.addAttribute("bookIdToRemove", bookIdToRemove);
            model.addAttribute("regexToRemoveBook", new RegexToRemoveBook());
            model.addAttribute("bookList", bookService.getAllBooks());
            return "book_shelf";
        }else {
            bookService.removeBookById(bookIdToRemove.getId());
            return "redirect:/books/shelf";
        }

    }

    @PostMapping("/removeByRegex")
    public String removeByRegex(@Valid RegexToRemoveBook regex, BindingResult bindingResult, Model model) throws BookShelfRegexException {
        if (bindingResult.hasErrors()){
            model.addAttribute("book", new Book());
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("regexToRemoveBook", regex);
            model.addAttribute("bookList", bookService.getAllBooks());
            return "book_shelf";
        }else {
            bookService.removeByRegex(regex.getRegex());
            return "redirect:/books/shelf";
        }
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file")MultipartFile file) throws BookShelfFileUploadException {

        String name = file.getOriginalFilename();
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
        } catch (Exception e) {
            throw new BookShelfFileUploadException("invalid upload file");
        }

        String rootPath = System.getProperty("catalina.home");
        File dir = new File(rootPath + File.separator + "external_uploads");

        if (!dir.exists()){
            dir.mkdirs();
        }

        File serverFile = new File(dir.getAbsoluteFile() + File.separator + name);
        BufferedOutputStream stream = null;
        try {
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            throw new BookShelfFileUploadException("invalid upload file");
        }

        logger.info("new file saved at: " + serverFile.getAbsolutePath());
        return "redirect:/books/shelf";
    }

    private void defaultInit() {
        logger.info("default INIT in book controller bean");
    }

    private void defaultDestroy() {
        logger.info("default DESTROY in book controller bean");
    }

    @ExceptionHandler(BookShelfFileUploadException.class)
    public String fileUploadError(Model model, BookShelfFileUploadException exception){
        model.addAttribute("errorFileUploadMessage", exception.getMessage());
        model.addAttribute("book", new Book());
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("regexToRemoveBook", new RegexToRemoveBook());
        model.addAttribute("bookList", bookService.getAllBooks());
        return "book_shelf";
    }

    @ExceptionHandler
    public String regexSyntaxError(Model model, BookShelfRegexException exception){
        model.addAttribute("errorRegexMessage", exception.getMessage());
        model.addAttribute("book", new Book());
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("regexToRemoveBook", new RegexToRemoveBook());
        model.addAttribute("bookList", bookService.getAllBooks());
        return "book_shelf";
    }


}
